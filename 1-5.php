<html>

<head>
    <title>Input date. Then show 3 days from inputted date and its day of the week</title>
</head>

<body>

    <form method="POST" action="">
        Enter Number: <input type="date" name="date1" />
        <input type="submit" name="submit_btn" value="Submit" />
    </form>

    <br>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST['date1']) && trim($_POST['date1']) != '') {

            $date = $_POST['date1'];

            //Our YYYY-MM-DD date string.
            $date2 = date('Y-m-d', strtotime($date . ' +1 day'));;
            $date3 = date('Y-m-d', strtotime($date2 . ' +1 day'));

            //Get the day of the week using PHP's date function.
            //Convert the date string into a unix timestamp.
            $dayOfWeek1 = date("l", strtotime($date));
            $dayOfWeek2 = date("l", strtotime($date2));
            $dayOfWeek3 = date("l", strtotime($date3));

            //Print out the day that our date fell on.
            echo 'Day1 ' . $date . ' fell on a ' . $dayOfWeek1 . '<br>';
            echo 'Day2 ' . $date2 . ' fell on a ' . $dayOfWeek2 . '<br>';
            echo 'Day3 ' . $date3 . ' fell on a ' . $dayOfWeek3 . '<br>';
        }
    }
    ?>

</body>

</html>