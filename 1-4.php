<html>

<head>
    <title>Solve FizzBuzz problem</title>
</head>

<body>

    <form method="POST" action="">
        Enter Number: <input type="text" name="num1" />
        <input type="submit" name="submit_btn" value="Submit" />
    </form>

    <br>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST['num1'];


        for ($i = 1; $i <= $num1; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                echo  " FizzBuzz" . "<br>";
            } else if ($i % 3 == 0) {
                echo  " Fizz" . "<br>";
            } else if ($i % 5 == 0) {
                echo  " Buzz" . "<br>";
            } else {
                echo $i . "<br>";
            }
        }
    }
    ?>

</body>

</html>