<!DOCTYPE html>
<html>
<head>
    <title>Show all data from CSV</title>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }
</style>
</head>

<body>
    <center>
        <h1>DISPLAY DATA PRESENT IN CSV</h1>

        <a href="register.php">Register</a>
        <br /><br />
        <?php


        echo "<table>";

        $col = 1;
        $counter = 0;
        $ro = 2;
        // open file
        $file = fopen("user_data1.9.csv", "r");
        if (!$file) {
        } else {
            while (($row = fgetcsv($file)) !== FALSE) {
                $counter++;

                if (1 == $counter) {
                    echo "<tr>";
                    echo "<th>ID</th>";
                    echo "<th>First Name</th>";
                    echo "<th>Middle Name</th>";
                    echo "<th>Last Name</th>";
                    echo "<th>Age</th>";
                    echo "<th>Email</th>";
                    echo "</tr> \n";
                }
                if ($counter > 0) {

                    echo "<tr>";
                    echo "<td>" . $row[0] . "</td>";
                    echo "<td>" . $row[1] . "</td>";
                    echo "<td>" . $row[2] . "</td>";
                    echo "<td>" . $row[3] . "</td>";
                    echo "<td>" . $row[4] . "</td>";
                    echo "<td>" . $row[5] . "</td>";
                    echo "</tr> \n";
                }
            }
        }

        echo "\n</table><br>";



        ?>
    </center>
</body>

</html>