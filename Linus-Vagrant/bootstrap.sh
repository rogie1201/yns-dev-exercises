#!/usr/bin/env bash

yum update -y
yum -y install httpd

echo "bootstrap.sh: Search and Get EPEL Repository on CentOS 8/7/6."
yum search epel-release
yum info epel-release
yum install epel-release

# PHP Mods
yum install -y php7.2-common
yum install -y php7.2-mcrypt
yum install -y php7.2-zip

yum install -y yum-utils
yum install -y php
yum install -y mysql-server

yum -y install phpmyadmin

echo "bootstrap.sh: Starting HTTP(PHPMyadmin)."
service httpd start
service mysqld start

echo "bootstrap.sh: Creating HTML directory."
mkdir html
echo "Initial set up provision done."

  