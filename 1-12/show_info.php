<html>

<head>
  <title>Information | Pagination Feature</title>
  <style>
    .success {
      color: #008000;
    }
  </style>
</head>

<body>
  <center>
    <h1>User Information</h1>
    <br>
    <a href="main.php">Show all</a>
    <br> <br>
    <?php

    $target_dir = "../uploads/";
    $target_file = $target_dir . basename($_FILES["user_image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
      $check = getimagesize($_FILES["user_image"]["tmp_name"]);
      if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
      } else {
        echo "File is not an image.";
        $uploadOk = 0;
      }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["user_image"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
    }

    // Allow certain file formats
    if (
      $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif"
    ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES["user_image"]["tmp_name"], $target_file)) {
        echo "<span class='success'>The file " . htmlspecialchars(basename($_FILES["user_image"]["name"])) . " has been uploaded.</span><br>";
      } else {
        echo "Sorry, there was an error uploading your file.";
      }
    }

    session_start();

    $First_Name = "";
    $Middle_Name = "";
    $Last_Name = "";
    $age = "";
    $email = "";
    
    $valid_check1 = true;
    $valid_check2 = true;
    $valid_check3 = true;
    $valid_check4 = true;

    function check_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

    if (isset($_POST['submit_btn'])) {

      $First_Name = check_input($_POST['First_Name']);
      $Middle_Name = check_input($_POST['Middle_Name']);
      $Last_Name = check_input($_POST['Last_Name']);
      $age = check_input($_POST['age']);
      $email = check_input($_POST['email']);

      if (empty($First_Name)) {
        $valid_check1 = false;
        $_SESSION['valid_check1'] =  1;
      } else {
        unset($_SESSION['valid_check1']);
      }

      if (empty($Last_Name)) {
        $valid_check2 = false;
        $_SESSION['valid_check2'] =  2;
      } else {
        unset($_SESSION['valid_check2']);
      }

      if (!is_numeric($age)) {
        $valid_check3 = false;
        $_SESSION['valid_check3'] =  3;
      } else {
        unset($_SESSION['valid_check3']);
      }

      if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) {
        $valid_check4 = false;
        $_SESSION['valid_check4'] =  4;
      } else {
        unset($_SESSION['valid_check4']);
      }

      if ($valid_check1 == false || $valid_check2 == false || $valid_check3 == false || $valid_check4 == false) {
        header("location:register.php");
      }

    }

    if ($valid_check1 && $valid_check2 && $valid_check3 && $valid_check4) {

      $file_open = fopen("user_data1.12.csv", "a");
      $no_rows = count(file("user_data1.12.csv"));
      if ($no_rows > 1) {
        $no_rows = ($no_rows - 1) + 1;
      }

      $form_data = array(
        'sr_no'  => $no_rows,
        'Image' => $target_file,
        'FirstName'  => $First_Name,
        'MiddleName'  => $Middle_Name,
        'LastName' => $Last_Name,
        'age' => $age,
        'email' => $email,
      );
      fputcsv($file_open, $form_data);

    ?>

      <br>
      <label>Image: </label>
      <image width='50' height='50' src=<?php echo "" . $target_file . ""; ?>>
        </td>

        <br>
        <label>First Name: </label>
        <?php echo $First_Name; ?>

        <br>
        <label>Middle Name: </label>
        <?php echo $Middle_Name; ?>

        <br>
        <label>Last Name: </label>
        <?php echo $Last_Name; ?>

        <br>
        <label>Age: </label>
        <?php echo $age; ?>

        <br>
        <label>Email: </label>
        <?php echo $email; ?>

      <?php } ?>
  </center>

</body>

</html>