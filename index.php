<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>YNS Exercises</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>YNS Exercises</h3>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                    <a href="#php_exer" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">HTML & PHP</a>
                    <ul class="collapse list-unstyled" id="php_exer">

                        <li><a href="#" id="1_1">Exercise 1-1 Show Hello World</a></li>
                        <li><a href="#" id="1_2">Exercise 1-2 The four basic operations of arithmetic</a></li>
                        <li><a href="#" id="1_3">Exercise 1-3 Show the greatest common divisor</a></li>
                        <li><a href="#" id="1_4">Exercise 1-4 Solve FizzBuzz problem</a></li>
                        <li><a href="#" id="1_5">Exercise 1-5 show 3 days from inputted date</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#php_exer6_13" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exer 1-6 to 1-13</a>
                    <ul class="collapse list-unstyled" id="php_exer6_13">
                        <li>
                            <a href="#" id="1_6">Exercise 1-6 Input user information & show in next page.</a>
                        </li>
                        <li>
                            <a href="#" id="1_7">Exercise 1-7 Validation in the user information</a>
                        </li>
                        <li>
                            <a href="#" id="1_8">Exercise 1-8 Save user information to CSV file</a>
                        </li>

                        <li><a href="#" id="1_9">Exercise 1-9 Show User information using table tags</a></li>
                        <li><a href="#" id="1_10">Exercise 1-10 Upload images</a></li>
                        <li><a href="#" id="1_11">Exercise 1-11 Show uploaded images in the table</a></li>
                        <li><a href="#" id="1_12">Exercise 1-12 pagination in the list page</a></li>
                        <li><a href="#" id="1_13">Exercise 1-13 login form and embed it into the system</a></li>


                    </ul>
                </li>
                <li>
                    <a href="#javascript" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">JavaScript</a>
                    <ul class="collapse list-unstyled" id="javascript">
                        <li><a href="#" id="2_1">Exercise 2-1 Show alert</a></li>
                        <li><a href="#" id="2_2">Exercise 2-2 Confirm dialog and redirection</a></li>
                        <li><a href="#" id="2_3">Exercise 2-3 The four basic operations of arithmetic</a></li>
                        <li><a href="#" id="2_4">Exercise 2-4 Show prime numbers</a></li>
                        <li><a href="#" id="2_5">Exercise 2-5 characters in text box and show it in label</a></li>
                        <li><a href="#" id="2_6">Exercise 2-6 Press button and add a label</a></li>
                        <li><a href="#" id="2_7">Exercise 2-7 Alert will tell you file name of the image</a></li>
                        <li><a href="#" id="2_8">Exercise 2-8 Show alert when you click link</a></li>
                        <li><a href="#" id="2_9">Exercise 2-9 Change text and background color</a></li>
                        <li><a href="#" id="2_10">Exercise 2-10 Scroll screen when you press buttons</a></li>
                        <li><a href="#" id="2_11">Exercise 2-11 Change background color using animation</a></li>
                        <li><a href="#" id="2_12">Exercise 2-12 Different image when mouse over and out</a></li>
                        <li><a href="#" id="2_13">Exercise 2-13 Change size of images </a></li>
                        <li><a href="#" id="2_14">Exercise 2-14 Show images base on combo box</a></li>
                        <li><a href="#" id="2_15">Exercise 2-15 Show current date and time in real time</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" id="6_13">Exer 1-6 to 1-13 With Mysql Dabatase</a>
                </li>
                <li>
                    <a href="#" id="5_1">Quiz</a>
                </li>
                <li>
                    <a href="#" id="5_2">Calendar</a>
                </li>
                <li>
                    <a href="#advsql" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Advance SQL</a>
                    <ul class="collapse list-unstyled" id="advsql">
                        <li><a href="#" id="7_1">Exercise 7-1 Selecting by age using birth date</a></li>
                        <li><a href="#" id="7_2">Exercise 7-2 daily work shifts</a></li>
                        <li><a href="#" id="7_3">Exercise 7-3 Using Case conditional statement</a></li>
                        <li><a href="#" id="7_4">Exercise 7-4 The four basic operations of arithmetic</a></li>
                
                    </ul>
                </li>
                <li>
                    <a href="#" id="myprofile">Profile</a>
                </li>
            </ul>

        </nav>

        <!-- Page Content  -->
        <div id="content">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" id="page_iframe" src="/yns-dev-exercises/profile.php"></iframe>
            </div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').toggleClass('active');
            });
            var url = "/yns-dev-exercises/";

            $('#1_1').on('click', function() {
                urlChange(url + "1-1.php");
            });
            $('#1_2').on('click', function() {
                urlChange(url + "1-2.php");
            });
            $('#1_3').on('click', function() {
                urlChange(url + "1-3.php");
            });
            $('#1_4').on('click', function() {
                urlChange(url + "1-4.php");
            });
            $('#1_5').on('click', function() {
                urlChange(url + "1-5.php");
            });

            $('#1_6').on('click', function() {
                urlChange(url + "1-6-page1.php");
            });
            $('#1_7').on('click', function() {
                urlChange(url + "1-7-page1.php");
            });
            $('#1_8').on('click', function() {
                urlChange(url + "1-8-page1.php");
            });
            $('#1_9').on('click', function() {
                urlChange(url + "1-9/main.php");
            });
            $('#1_10').on('click', function() {
                urlChange(url + "1-10/register.php");
            });
            $('#1_11').on('click', function() {
                urlChange(url + "1-11/main.php");
            });
            $('#1_12').on('click', function() {
                urlChange(url + "1-12/main.php");
            });
            $('#1_13').on('click', function() {
                urlChange(url + "1-13/main.php");
            });


            $('#2_1').on('click', function() {
                urlChange(url + "Javascript/2-1.php")
            });
            $('#2_2').on('click', function() {
                urlChange(url + "Javascript/2-2.php");
            });
            $('#2_3').on('click', function() {
                urlChange(url + "Javascript/2-3.php");
            });
            $('#2_4').on('click', function() {
                urlChange(url + "Javascript/2-4.php");
            });
            $('#2_5').on('click', function() {
                urlChange(url + "Javascript/2-5.php");
            });
            $('#2_6').on('click', function() {
                urlChange(url + "Javascript/2-6.php");
            });
            $('#2_7').on('click', function() {
                urlChange(url + "Javascript/2-7.php");
            });
            $('#2_8').on('click', function() {
                urlChange(url + "Javascript/2-8.php");
            });
            $('#2_9').on('click', function() {
                urlChange(url + "Javascript/2-9.php");
            });
            $('#2_10').on('click', function() {
                urlChange(url + "Javascript/2-10.php");
            });
            $('#2_11').on('click', function() {
                urlChange(url + "Javascript/2-11.php");
            });
            $('#2_12').on('click', function() {
                urlChange(url + "Javascript/2-12.php");
            });
            $('#2_13').on('click', function() {
                urlChange(url + "Javascript/2-13.php");
            });
            $('#2_14').on('click', function() {
                urlChange(url + "Javascript/2-14.php");
            });
            $('#2_15').on('click', function() {
                urlChange(url + "Javascript/2-15.php");
            });


            $('#6_13').on('click', function() {
                urlChange(url + "Database/3-5/login.php");
            });
            $('#5_1').on('click', function() {
                urlChange(url + "5-1/index.php");
            });
            $('#5_2').on('click', function() {
                urlChange(url + "5-2/index.php");
            });

            $('#myprofile').on('click', function() {
                urlChange(url + "profile.php");
            });

            $('#7_1').on('click', function() {
                urlChange(url + "7-1.php");
            });

            $('#7_2').on('click', function() {
                urlChange(url + "7-2.php");
            });

            $('#7_3').on('click', function() {
                urlChange(url + "7-3.php");
            });

            $('#7_4').on('click', function() {
                urlChange(url + "7-4.php");
            });
            
        });

        function urlChange(url) {
            document.getElementById('page_iframe').src = url;
        }
    </script>
</body>

</html>