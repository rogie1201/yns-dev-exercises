<html>

<head>
    <title>Validation</title>
</head>

<h1>User Information</h1>

<body>
    <?php

    session_Start();

    $First_Name = "";
    $Middle_Name = "";
    $Last_Name = "";
    $age = "";
    $email = "";
   
    $valid_check1 = true;
    $valid_check2 = true;
    $valid_check3 = true;
    $valid_check4 = true;

    function check_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if (isset($_POST['submit_btn'])) {

        $First_Name = check_input($_POST['First_Name']);
        $Middle_Name = check_input($_POST['Middle_Name']);
        $Last_Name = check_input($_POST['Last_Name']);
        $age = check_input($_POST['age']);
        $email = check_input($_POST['email']);

        if (empty($First_Name)) {
            $valid_check1 = false;
            $_SESSION['valid_check1'] =  1;
        } else {
            unset($_SESSION['valid_check1']);
        }

        if (empty($Last_Name)) {
            $valid_check2 = false;
            $_SESSION['valid_check2'] =  2;
        } else {
            unset($_SESSION['valid_check2']);
        }

        if (!is_numeric($age)) {
            $valid_check3 = false;
            $_SESSION['valid_check3'] =  3;
        } else {
            unset($_SESSION['valid_check3']);
        }

        if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) {
            $valid_check4 = false;
            $_SESSION['valid_check4'] =  4;
        } else {
            unset($_SESSION['valid_check4']);
        }

        if ($valid_check1 == false || $valid_check2 == false || $valid_check3 == false || $valid_check4 == false) {
            header("location:1-7-page1.php");
        } 
    }

    if ($valid_check1 && $valid_check2 && $valid_check3 && $valid_check4) {
    ?>

        <label>First Name: </label>
        <?php echo $First_Name; ?>

        <br>
        <label>Middle Name: </label>
        <?php echo $Middle_Name; ?>

        <br>
        <label>Last Name: </label>
        <?php echo $Last_Name; ?>

        <br>
        <label>Age: </label>
        <?php echo $age; ?>

        <br>
        <label>Email: </label>
        <?php echo $email; ?>

    <?php } ?>
</body>

</html>