<!doctype html>
<html lang="en">

<head>
    <title>Exer 7-3</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {
        display: inline;
    }

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }
</style>

<body>
    <?php
    $connect = mysqli_connect("localhost", "root", "", "employee_db") or die("Cannot connect to Database");

    $sql = "SELECT A.first_name, A.last_name, A.middle_name,
            CASE C.name
            WHEN 'CEO' THEN 'Chief Executive Officer'
            WHEN 'CTO' THEN 'Chief Technical Officer'
            WHEN 'CFO' THEN 'Chief Financial Officer'
            ELSE C.name
            END AS name
            FROM employees A
            INNER JOIN employee_positions B
                ON A.id = B.employee_id
            INNER JOIN positions C
                ON B.position_id = C.id
            ";
    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);

    ?>

    <table>
        <thead>
            <tr>
                <th colspan="100%">
                    <h4>Using same data as SQL problems. Get employees information with full position name. With Case conditional statement</h6>
                </th>
            <tr>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">First Name</th>
                <th scope="col">Middle Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Position</th>
             
            </tr>
        </thead>
        <tbody>
            <?php
            $cntr = 1;
            if ($count > 0) {
                while ($row = mysqli_fetch_array($query)) { ?>
                    <tr>
                        <th><?= $cntr ?></th>
                        <td><?= $row['first_name'] ?></td>
                        <td><?= $row['middle_name'] ?></td>
                        <td><?= $row['last_name'] ?></td>
                        <td><?= $row['name'] ?></td>
                      
                    </tr>
            <?php
                    $cntr++;
                }
            }
            ?>
        </tbody>
    </table>
</body>

</html>