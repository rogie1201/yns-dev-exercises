<?php

session_Start();

if (isset($_SESSION['session_id'])) {
    header("location:main.php");
}

$login_error = '';


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST['username'];
    $password = $_POST['password'];
    $csv = "user_data1.13.csv";
    $success = false;
    if (file_exists($csv)) {
        $handle = fopen("user_data1.13.csv", "r") or die("file dont exist");
        while (($data = fgetcsv($handle)) !== FALSE) {
            if ($data[7] == $username && $data[8] == $password) {
                $success = true;
                break;
            }
        }
        fclose($handle);

        if ($success) {
            // they logged in ok
            $_SESSION['session_id'] =  $username;

            header("location:main.php");
        } else {
            // login failed
            $login_error = "You have entered an invalid username and/or password, Please try again.";
        }
    } else {
        $login_error = "You have entered an invalid username and/or password, Please try again."; 
    }
}


?>
<!DOCTYPE html>
<html>

<head>
    <title>Log in</title>
    <style>
        .error {
            color: #FF0000;
        }
    </style>
</head>

<body>
    <center>
        <h3>Sign in to start your session</h3>

        <form method="POST" action="">
            <label>Username</label>
            <br>
            <input type="text" name="username" required="true">
            <br>
            <label>Password</label>
            <br>
            <input type="password" name="password" required="true">
            <br /><br />
            <input type="submit" name="submit_btn" value="Sign in" />

            <br />

            <span class="error" id="login_error"><?php echo $login_error; ?></span>

        </form>
        <br>
        <a href="register.php"><input type="button" name="submit_btn" value="Register" /></a>
    </center>

</body>

</html>