<!DOCTYPE html>
<html>
<head>
    <title>Show All</title>
<?php
session_Start();

if (!isset($_SESSION['session_id'])) {
    header("location:index.php");
}
?>

<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }
</style>
</head>

<body>
    <center>
        <h2>DISPLAY USER DATA IN CSV</h2>
        <a href="register.php">Register</a>
        &nbsp;|&nbsp;
        <a href="logout.php">Logout - <?php echo  $_SESSION['session_id']; ?></a>
        <br />
        <br />
        <?php
        echo "<table>";
        // open file
        $csv_data = array();
        $newcsv_data = array();
        $counter = 1;
        $file =   fopen("user_data1.13.csv", "r") or die("file dont exist");
        if (!$file) {
        } else {
            echo "<tr>";
            echo "<th>No.</th>";
            echo "<th>Image</th>";
            echo "<th>First Name</th>";
            echo "<th>Middle Name</th>";
            echo "<th>Last Name</th>";
            echo "<th>Age</th>";
            echo "<th>Email</th>";
            echo "</tr>";
            while (($arr = fgetcsv($file)) !== FALSE) {
                $csv_data = $arr;
                array_push($newcsv_data, $csv_data);
            }
            $page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
            $total = count($newcsv_data);
            $limit = 10;
            $totalPages = ceil($total / $limit);
            $page = max($page, 1);
            $page = min($page, $totalPages);
            $offset = ($page - 1) * $limit;
            if ($offset < 0) $offset = 0;
            $newcsv_data = array_slice($newcsv_data, $offset, $limit);
            foreach ($newcsv_data as $row) {
                echo "<tr>";
                echo "<td>" . $counter . "</td>";
                echo "<td><image width='50' height='50' src='" . $row[1] . "'></td>";
                echo "<td>" . $row[2] . "</td>";
                echo "<td>" . $row[3] . "</td>";
                echo "<td>" . $row[4] . "</td>";
                echo "<td>" . $row[5] . "</td>";
                echo "<td>" . $row[6] . "</td>";
                echo "</tr>";
                $counter++;
            }
        }
        echo "</table><br><br>";

        $link = 'main.php?page=%d';
        $pageContainer = "<div>";
        if ($totalPages != 0) {
            if ($page == 1) {
                $pageContainer .= "";
            } else {
                $pageContainer .= sprintf('<a href="' . $link . '"> < Prev Page</a>', $page - 1);
            }
            $pageContainer .= ' <span> Page <strong>' . $page . '</strong> of ' . $totalPages . '</span>';
            if ($page == $totalPages) {
                $pageContainer .= '';
            } else {
                $pageContainer .= sprintf('<a href="' . $link . '"> Next Page > </a>', $page + 1);
            }
        }
        $pageContainer .= '</div> <br>';
        echo $pageContainer;

        ?>
    </center>
</body>

</html>