<html>

<head>
    <title>Register | Show Uploaded Images</title>
    <style>
        .error {
            color: #FF0000;
        }
    </style>
</head>

<body>
    <center>
        <a href="main.php">Show all</a>
        <h1>Enter User Information</h1>


        <?php
        session_start();
        $valid_fname = "";
        $valid_lname = "";
        $valid_age = "";
        $valid_email = "";

        if (isset($_SESSION['valid_check1'])) {
            $valid_fname = "Please enter first name.";
        }
        if (isset($_SESSION['valid_check2'])) {
            $valid_lname = "Please enter last name.";
        }
        if (isset($_SESSION['valid_check3'])) {
            $valid_age = "Age must be a number.";
        }
        if (isset($_SESSION['valid_check4'])) {
            $valid_email = "You have entered an invalid email address!";
        }


        ?>

        <form method="POST" action="show_info.php" enctype="multipart/form-data">

            <label>Image </label>
            <br>
            <input type="file" name="user_image" required="true">
            <br>
            <br>

            <label>First Name</label>
            <br>
            <input type="text" name="First_Name">
            <br>
            <span class="error"><?php echo $valid_fname; ?></span>

            <br>
            <label>Middle Name (Optional)</label>
            <br>
            <input type="text" name="Middle_Name">

            <br>
            <label>Last Name</label>
            <br>
            <input type="text" name="Last_Name">
            <br>
            <span class="error"><?php echo $valid_lname; ?></span>
            <br>

            <label>Age</label>
            <br>
            <input type="number" name="age">
            <br>
            <span class="error"><?php echo $valid_age; ?></span>
            <br>
            <label>Email</label>
            <br>
            <input type="email" name="email" onchange="ValidateEmail(this)">
            <br>
            <span class="error" id="email_error"><?php echo $valid_email; ?></span>
            <br>

            <br>

            <input type="submit" name="submit_btn" value="Submit" />
        </form>
    </center>
</body>

</html>

<script>
    function ValidateEmail(inputText) {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (inputText.value.match(mailformat)) {
            document.getElementById("email_error").innerHTML = "";
            return true;
        } else {
            document.getElementById("email_error").innerHTML = "You have entered an invalid email address!";
            return false;
        }
    }
</script>