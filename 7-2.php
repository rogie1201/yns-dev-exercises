<!doctype html>
<html lang="en">

<head>
    <title>Exer 7-2</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {
        display: inline;
    }

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }
</style>

<body>
    <?php
    $connect = mysqli_connect("localhost", "root", "", "employee_db") or die("Cannot connect to Database");

    $sql = "Select a.therapist_id, b.name, a.target_date, a.start_time, a.end_time, 
    DATE_FORMAT(a.start_time,'%H:%i:%s') as stime,
    DATE_FORMAT(a.end_time,'%H:%i:%s') as etime,
         CASE 
            WHEN a.start_time <= '2021-02-10 05:59:59' AND a.start_time >= '2021-02-10 00:00:00'
                THEN  CONCAT(DATE_ADD(a.target_date, INTERVAL 1 DAY),' ',DATE_FORMAT(a.start_time,'%H:%i:%s')) 
                ELSE CONCAT(a.target_date,' ',DATE_FORMAT(a.start_time,'%H:%i:%s')) 
            END
        AS sort_start_time
    
    from daily_works_shifts a inner join therapists b on a.therapist_id = b.id  
    
    order by a.target_date, sort_start_time ASC";

    //echo  $sql;

    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);

    ?>
    <table>
        <thead>
            <tr>
                <th colspan="100%">
                    <h4>Exercise 7-2 (List all therapists according to their daily work shifts)</h6>
                </th>
            <tr>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Therapist name</th>
                <th scope="col">Target date</th>
                <th scope="col">Start time</th>
                <th scope="col">End time</th>
                <th scope="col"> Sort Start time</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $cntr = 1;
            if ($count > 0) {
                while ($row = mysqli_fetch_array($query)) { ?>
                    <tr>
                        <td><?= $cntr ?></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['target_date'] ?></td>
                        <td><?= $row['stime'] ?></td>
                        <td><?= $row['etime'] ?></td>
                        <td><?= $row['sort_start_time'] ?></td>
                    </tr>
            <?php
                    $cntr++;
                }
            }
            ?>
        </tbody>
    </table>
</body>

</html>