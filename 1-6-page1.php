<html>

<head>
    <title>Put user information. Then show it in next page.</title>
    <style>
        .error {
            color: #FF0000;
        }
    </style>
</head>

<h1>Enter User Information</h1>

<body>

    <form method="POST" action="1-6-page2.php">
        <label>First Name</label>
        <br>
        <input type="text" name="First_Name">

        <br>
        <label>Middle Name (Optional)</label>
        <br>
        <input type="text" name="Middle_Name">

        <br>
        <label>Last Name</label>
        <br>
        <input type="text" name="Last_Name">
        <br>

        <label>Age</label>
        <br>
        <input type="number" name="age">

        <br>
        <label>Email</label>
        <br>
        <input type="email" name="email">
        <br>

        <br>

        <input type="submit" name="submit_btn" value="Submit" />
    </form>

</body>
</html>