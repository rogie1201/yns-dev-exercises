<html>

<head>
    <title>Put user information. Then show it in next page.</title>
    <style>
        .error {
            color: #FF0000;
        }
    </style>
</head>

<body>
<center>
        <a href="main_page.php">Show all</a>
        <h1>Enter User Information</h1>
        <form method="POST" action="display_info.php" enctype="multipart/form-data">

            <label>Image </label>
            <br>
            <input type="file" name="user_image" required="true">
            <br>
            <br>

            <label>First Name</label>
            <br>
            <input type="text" name="First_Name" required="true">
            <br>
            <br>
            
            <label>Middle Name (Optional)</label>
            <br>
            <input type="text" name="Middle_Name">
            <br>
            <br>

            <label>Last Name</label>
            <br>
            <input type="text" name="Last_Name" required="true">
            <br>
            <br>

            <label>Age</label>
            <br>
            <input type="number" name="age">
            <br>
            <br>
            
            <label>Email</label>
            <br>
            <input type="email" name="email" onchange="ValidateEmail(this)">
            <br>
            <span class="error" id="email_error"></span>
            <br>
            <br>

            <label>Username</label>
            <br>
            <input type="text" name="username">
            <br>
            <br>

            <label>Password</label>
            <br>
            <input type="password" name="password">
            <br>
            <br>

            <input type="submit" name="submit_btn" value="Submit" />
        </form>
        </center>
</body>

</html>

<script>
    function ValidateEmail(inputText) {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (inputText.value.match(mailformat)) {
            document.getElementById("email_error").innerHTML = "";
            return true;
        } else {
            document.getElementById("email_error").innerHTML = "You have entered an invalid email address!";
            return false;
        }
    }
</script>