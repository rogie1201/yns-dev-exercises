<!DOCTYPE html>
<?php
session_Start();
require_once 'db.php';
if (isset($_SESSION['session_id'])) {

    $s = "select username from tbluserinfo where tbUserInfoId = '" . $_SESSION['session_id'] . "'";
    $query = mysqli_query($connect, $s);
    $count = mysqli_num_rows($query);
    $row = mysqli_fetch_array($query);

} else {
    header("location:login.php");
}
?>
<html>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {
        display: inline;
    }

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }
</style>
</head>

<body>
    <center>
        <h2>DISPLAY USER DATA FROM MYSQL</h2>
        <a href="register.php">New User</a>
        &nbsp;|&nbsp;
        <a href="logout.php">Logout - <?php echo  $row['username']; ?></a>
        <br />
        <br />
        <?php

        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }

        $counter = 1;
        $no_of_records_per_page = 10;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $total_pages_sql = "SELECT COUNT(*) FROM tbluserinfo";
        $result = mysqli_query($connect, $total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $sql = "SELECT * FROM tbluserinfo LIMIT $offset, $no_of_records_per_page";

        echo "<html><body><center><table>";

        $query = mysqli_query($connect, $sql);
        $count = mysqli_num_rows($query);

        echo "<tr>";
        echo "<th>No.</th>";
        echo "<th>Image</th>";
        echo "<th>First Name</th>";
        echo "<th>Middle Name</th>";
        echo "<th>Last Name</th>";
        echo "<th>Age</th>";
        echo "<th>Email</th>";
        echo "<th>Username</th>";
        echo "<th>Password</th>";
        echo "</tr>";

        if ($count > 0) {
            while ($row = mysqli_fetch_array($query)) {
                
                echo "<tr>";
                echo "<td>" . $counter . "</td>";
                echo "<td><image width='50' height='50' src='" . $row['image'] . "'></td>";
                echo "<td>" . $row['firstName'] . "</td>";
                echo "<td>" . $row['middleName'] . "</td>";
                echo "<td>" . $row['lastName'] . "</td>";
                echo "<td>" . $row['age'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
                echo "<td>" . $row['username'] . "</td>";
                echo "<td>" . $row['password'] . "</td>";
                echo "</tr>";
                $counter++;
            }
        }
        echo "</table></center></body></html><br>";
        ?>
        <ul class="pagination">
            <li><a href="?pageno=1">First</a></li>
            <li class="<?php if ($pageno <= 1) {
                            echo 'disabled';
                        } ?>">
                <a href="<?php if ($pageno <= 1) {
                                echo '#';
                            } else {
                                echo "?pageno=" . ($pageno - 1);
                            } ?>">Prev</a>
            </li>
            <li class="<?php if ($pageno >= $total_pages) {
                            echo 'disabled';
                        } ?>">
                <a href="<?php if ($pageno >= $total_pages) {
                                echo '#';
                            } else {
                                echo "?pageno=" . ($pageno + 1);
                            } ?>">Next</a>
            </li>
            <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
        </ul>
    </center>
</body>

</html>