<?php

session_Start();

require_once 'db.php';

if (isset($_SESSION['session_id'])) {
    header("location:main_page.php");
}

$login_error = '';


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = mysqli_real_escape_string($connect, $_POST['username']);
    $password = mysqli_real_escape_string($connect, $_POST['password']);

    $success = false;

    $sql = "select tbUserInfoId from tbluserinfo where username = '" . $username . "' and password = '" . $password . "'";
    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);

    if ($count > 0) {
        // they logged in ok
        $row = mysqli_fetch_array($query);
        $_SESSION['session_id'] = $row['tbUserInfoId'];
        header("location:main_page.php");
    } else {
        // login failed
        $login_error = "You have entered an invalid username and/or password, Please try again.";
    }
}


?>
<!DOCTYPE html>
<html>

<head>
    <title>Log in</title>
    <style>
        .error {
            color: #FF0000;
        }
    </style>
</head>

<body>
    <center>
        <h3>Sign in to start your session</h3>

        <form method="POST" action="">
            <label>Username</label>
            <br>
            <input type="text" name="username" required="true">
            <br>
            <label>Password</label>
            <br>
            <input type="password" name="password" required="true">
            <br /><br />
            <input type="submit" name="submit_btn" value="Sign in" />

            <br /><br />

            <span class="error" id="login_error"><?php echo $login_error; ?></span>

        </form>

        <a href="register.php"><input type="button" name="" value="Register" /></a>


    </center>

</body>

</html>