<html>

<head>
  <title>Display User Info</title>
  <style>
    .success {
      color: #008000;
    }
  </style>
</head>

<body>
  <center>
    <h1>User Information</h1>
    <br>
    <a href="main_page.php">Show all</a>
    <br> <br>
    <?php

    session_Start();
    require_once 'db.php';


    $target_dir = "../../uploads/";
    $target_file = $target_dir . basename($_FILES["user_image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
      $check = getimagesize($_FILES["user_image"]["tmp_name"]);
      if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
      } else {
        echo "File is not an image.";
        $uploadOk = 0;
      }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["user_image"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
    }

    // Allow certain file formats
    if (
      $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif"
    ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES["user_image"]["tmp_name"], $target_file)) {
        echo "<span class='success'>The file " . htmlspecialchars(basename($_FILES["user_image"]["name"])) . " has been uploaded.</span><br>";
      } else {
        echo "Sorry, there was an error uploading your file.";
      }
    }


    if (isset($_POST['First_Name']) && trim($_POST['First_Name']) != '') {
      $First_Name = $_POST['First_Name'];
    }

    if (isset($_POST['Middle_Name']) && trim($_POST['Middle_Name']) != '') {
      $Middle_Name = $_POST['Middle_Name'];
    }

    if (isset($_POST['Last_Name']) && trim($_POST['Last_Name']) != '') {
      $Last_Name = $_POST['Last_Name'];
    }

    if (isset($_POST['age']) && trim($_POST['age']) != '') {
      $age = $_POST['age'];
    }

    if (isset($_POST['email']) && trim($_POST['email']) != '') {
      $email = $_POST['email'];
    }

    if (isset($_POST['username']) && trim($_POST['username']) != '') {
      $username = $_POST['username'];
    }

    if (isset($_POST['password']) && trim($_POST['password']) != '') {
      $password = $_POST['password'];
    }


    $insert = "INSERT INTO tbluserinfo (image, firstName, middleName, lastName, age, email, username, password) values ('" . $target_file . "','" .  $First_Name . "','" .  $Middle_Name . "','" .  $Last_Name . "'," .  $age . ",'" .  $email . "','" .  $username . "','" .  $password . "')";
    mysqli_query($connect, $insert) or die(mysqli_error($connect));
    $primary_id = mysqli_insert_id($connect);


    ?>
    <br>
    <label>First Name: </label>
    <?php echo $First_Name; ?>

    <br>
    <label>Middle Name: </label>
    <?php echo $Middle_Name; ?>

    <br>
    <label>Last Name: </label>
    <?php echo $Last_Name; ?>

    <br>
    <label>Age: </label>
    <?php echo $age; ?>

    <br>
    <label>Email: </label>
    <?php echo $email; ?>

    <br>
    <label>Username: </label>
    <?php echo $username; ?>

    <br>
    <label>Password: </label>
    <?php echo $password; ?>


  </center>

</body>

</html>