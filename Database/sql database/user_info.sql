-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2021 at 10:49 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbluserinfo`
--

CREATE TABLE `tbluserinfo` (
  `tbUserInfoId` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `firstName` varchar(250) NOT NULL,
  `middleName` varchar(250) NOT NULL,
  `lastName` varchar(250) NOT NULL,
  `age` int(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbluserinfo`
--

INSERT INTO `tbluserinfo` (`tbUserInfoId`, `image`, `firstName`, `middleName`, `lastName`, `age`, `email`, `username`, `password`, `createdDate`) VALUES
(1, '../../uploads/userpic.png', 'steve', 'J', 'Rogers', 35, 'steved@gmail.com', 'steve', '12345', '2021-02-03 10:52:46'),
(2, '../../uploads/femalepic.jpg', 'ana mae', 'g', 'ramos', 34, 'ana@gmail.comm', 'ana', '12345', '2021-02-03 10:53:36'),
(4, '../../uploads/z.png', 'z', 'z', 'z', 12, 'z@gmail.com', 'z', 'zzz', '2021-02-03 15:29:17'),
(5, '../../uploads/ann.jpg', 'ann', 'ann', 'ann', 34, 'ann@gmail.com', 'ann', '1234', '2021-02-03 15:51:08'),
(6, '../../uploads/david.jpg', 'david', 'd', 'd', 54, 'david@gmail.com', 'david', '1234', '2021-02-03 15:52:07'),
(7, '../../uploads/grace.jpg', 'grace', 'g', 'g', 24, 'grace@gmail.com', 'grace', '1234', '2021-02-03 15:52:40'),
(8, '../../uploads/martin.jpg', 'martin', 'm', 'm', 55, 'martin@gmail.com', 'martin', '1234', '2021-02-03 15:53:12'),
(9, '../../uploads/matt.jpg', 'matt', 'm', 'm', 35, 'matt@gmail.com', 'matt', '1234', '2021-02-03 15:53:37'),
(10, '../../uploads/mia.jpg', 'mia', 'm', 'm', 25, 'mia@gmail.com', 'mia', '1234', '2021-02-03 15:54:04'),
(11, '../../uploads/rob.jpg', 'rob', 'r', 'r', 45, 'rob@gmail.com', 'rob', '1234', '2021-02-03 15:54:41'),
(12, '../../uploads/jane.jpg', 'jane', 'j', 'j', 26, 'jane@gmail.com', 'jane', '1234', '2021-02-03 15:56:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbluserinfo`
--
ALTER TABLE `tbluserinfo`
  ADD PRIMARY KEY (`tbUserInfoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbluserinfo`
--
ALTER TABLE `tbluserinfo`
  MODIFY `tbUserInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
