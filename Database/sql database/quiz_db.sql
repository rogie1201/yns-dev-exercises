-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2021 at 04:51 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`id`, `question_id`, `value`) VALUES
(1, 1, 'flag pole'),
(2, 1, 'River'),
(3, 1, 'Stone'),
(4, 2, 'Corrupted Blood'),
(5, 2, 'COVID'),
(6, 2, 'Iloveyou virus'),
(7, 3, 'Minecraft'),
(8, 3, 'Pac-Man'),
(9, 3, 'MLBB'),
(10, 4, '100'),
(11, 4, '80'),
(12, 4, '50'),
(13, 5, 'Mortal Kombat'),
(14, 5, 'Super Mario'),
(15, 5, 'Grand Theft Auto: San Andreas'),
(16, 6, 'Michael Jackson'),
(17, 6, 'Ed Sheeran'),
(18, 6, 'Bruno Mars'),
(19, 7, '3DO'),
(20, 7, 'CDO'),
(21, 7, '2DO'),
(22, 8, '2017'),
(23, 8, '2016'),
(24, 8, '2015'),
(25, 9, 'Donkey Kong'),
(26, 9, 'King Kong'),
(27, 9, 'Minecraft'),
(28, 10, 'Ryan Reynolds'),
(29, 10, 'Will Smith'),
(30, 10, 'Johnny Depp');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answerid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `answerid`) VALUES
(1, 'What does Mario jump on when he completes a level?', 1),
(2, 'What was the name of the virtual pandemic that struck the \"World of Warcraft\" in 2005?', 4),
(3, 'Notch\" is the world-famous designer of which videogame?', 7),
(4, 'How many players can compete against each other simultaneously in a \"Fortnite Battle Royale\"?', 10),
(5, 'Ed Boon and John Tobias are the original designers of which renowned videogame franchise?', 13),
(6, 'Which famous entertainer composed music for \"Sonic 3\"?', 16),
(7, 'What was the first videogame console with a CD-ROM embedded into it?', 19),
(8, 'In what year was \"Fortnite\" released?', 22),
(9, 'Mario originated as a character in which classic videogame?', 25),
(10, 'Who provides the voice of Pikachu in the 2019 \"Pokémon: Detective Pikachu\" movie?', 28);

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE `score` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `score` int(11) NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_questions`
-- (See below for the actual view)
--
CREATE TABLE `vw_questions` (
);

-- --------------------------------------------------------

--
-- Structure for view `vw_questions`
--
DROP TABLE IF EXISTS `vw_questions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_questions`  AS SELECT `a`.`id` AS `id`, `a`.`question_id` AS `question_id`, `a`.`choice_count` AS `choice_count`, `b`.`question` AS `question`, `a`.`value` AS `value`, `b`.`answerid` AS `answerid` FROM (`choices` `a` join `questions` `b` on(`a`.`question_id` = `b`.`id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
