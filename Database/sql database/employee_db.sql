-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2021 at 09:47 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `createdDate`) VALUES
(1, 'Executive', '2021-02-03 11:46:12'),
(2, 'Admin', '2021-02-03 11:46:12'),
(3, 'Sales', '2021-02-03 11:46:27'),
(4, 'Development', '2021-02-03 11:46:27'),
(5, 'Design', '2021-02-03 11:46:43'),
(6, 'Marketing', '2021-02-03 11:46:43');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) NOT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `department_id` int(250) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `boss_id` int(250) DEFAULT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`, `createdDate`) VALUES
(1, 'Manabu', 'Yamazaki', NULL, '1976-03-15', 1, NULL, NULL, '2021-02-03 11:39:04'),
(2, 'Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1, '2021-02-03 11:43:46'),
(3, 'Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-04-01', 1, '2021-02-03 11:43:46'),
(4, 'Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1, '2021-02-03 11:43:46'),
(5, 'Lorraine', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1, '2021-02-03 11:43:46'),
(6, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2, '2021-02-03 11:43:46'),
(7, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1, '2021-02-03 11:43:46'),
(8, 'Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1, '2021-02-03 11:43:46'),
(9, 'Hideshi', 'Ogoshi', NULL, '1983-07-15', 4, '2014-06-01', 1, '2021-02-03 11:43:46'),
(10, 'Kim', '', '', '1977-10-16', 5, '2015-08-06', 1, '2021-02-03 11:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `employee_positions`
--

CREATE TABLE `employee_positions` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_positions`
--

INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`, `createdDate`) VALUES
(1, 1, 1, '2021-02-03 11:48:47'),
(2, 1, 2, '2021-02-03 11:48:47'),
(3, 1, 3, '2021-02-03 11:48:47'),
(4, 2, 4, '2021-02-03 11:48:47'),
(5, 3, 5, '2021-02-03 11:48:47'),
(6, 4, 5, '2021-02-03 11:50:12'),
(7, 5, 5, '2021-02-03 11:50:12'),
(8, 6, 5, '2021-02-03 11:50:12'),
(9, 7, 5, '2021-02-03 11:50:12'),
(10, 8, 5, '2021-02-03 11:50:12'),
(11, 9, 5, '2021-02-03 11:50:46'),
(12, 10, 5, '2021-02-03 11:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `master_data`
--

CREATE TABLE `master_data` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_data`
--

INSERT INTO `master_data` (`id`, `parent_id`) VALUES
(1, NULL),
(2, 5),
(3, NULL),
(4, 1),
(5, NULL),
(7, 3),
(8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `createdDate`) VALUES
(1, 'CEO', '2021-02-03 11:47:06'),
(2, 'CTO', '2021-02-03 11:47:06'),
(3, 'CFO', '2021-02-03 11:47:22'),
(4, 'Manager', '2021-02-03 11:47:22'),
(5, 'Staff', '2021-02-03 11:47:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_positions`
--
ALTER TABLE `employee_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_data`
--
ALTER TABLE `master_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employee_positions`
--
ALTER TABLE `employee_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `master_data`
--
ALTER TABLE `master_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
