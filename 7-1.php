<!doctype html>
<html lang="en">

<head>
    <title>Exer 7-1</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {
        display: inline;
    }

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }
</style>

<body>
    <?php
    $connect = mysqli_connect("localhost", "root", "", "employee_db") or die("Cannot connect to Database");

    $sql = "SELECT *, TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) AS age
        FROM employees 
        WHERE TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) > 30 
        AND TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) < 40 
        ORDER BY age";
    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);

    ?>

    <table>
        <thead>
            <tr>
                <th colspan="100%">
                    <h4>Get employees who is older than 30 years old but younger than 40 years old. (Result may vary based on current date)</h6>
                </th>
            <tr>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">First Name</th>
                <th scope="col">Middle Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Birthdate</th>
                <th scope="col">Age</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $cntr = 1;
            if ($count > 0) {
                while ($row = mysqli_fetch_array($query)) { ?>
                    <tr>
                        <th><?= $cntr ?></th>
                        <td><?= $row['first_name'] ?></td>
                        <td><?= $row['middle_name'] ?></td>
                        <td><?= $row['last_name'] ?></td>
                        <td><?= $row['birth_date'] ?></td>
                        <td><?= $row['age'] ?></td>
                    </tr>
            <?php
                    $cntr++;
                }
            }
            ?>
        </tbody>
    </table>
</body>

</html>