<!doctype html>
<html lang="en">

<head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Quiz</title>

</head>

<body>

    <?php
    session_Start();


    $username = "";
    $correct_answer = "";
    $selected_answer = "";
    $cnt = 1;
    $score_counter = 0;
    $qId = "";


    if (isset($_POST['username']) && trim($_POST['username']) != '') {
        $username = $_POST['username'];
        $_SESSION['username'] = $username;
    }

    require_once 'db.php';


    $sql = "SELECT * FROM questions order by RAND() limit 3";
    $query = mysqli_query($connect, $sql);
    $question_row = mysqli_fetch_array($query);
    $qId = $question_row['id'];

    $sql = "SELECT * FROM choices where question_id = " . $question_row['id'] . " order by RAND()";
    $query_choices = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query_choices);

    $query_questions = "SELECT * FROM questions";
    $questions = mysqli_query($connect, $query_questions);
    $total_questions = mysqli_num_rows($questions);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['selected_answer'])) {

            $correct_answer =   $_POST['correct_answer'];
            $selected_answer = $_POST['selected_answer'];
            $cnt = $_POST['answer_counter'] + 1;

            if ($selected_answer == $correct_answer) {
                $score_counter = $_POST['score_counter'] + 1;
            } else {
                $score_counter =  $_POST['score_counter'];
            }
        }
    }


    ?>
    <?php if ($cnt < 11) { ?>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Username | <?php echo $_SESSION['username'] . " - <b>Score</b> : " . $score_counter;   ?>
                </div>
                <div class="card-body">



                    <form method="POST" action="" id="question_form">
                        <div class="form-group">
                            <label for="choices"><?php echo $cnt . ". " . $question_row['question'];  ?></label>
                            <input type="hidden" class="form-check-input" name="correct_answer" value="<?php echo $question_row['answerid']; ?>">
                            <input type="hidden" class="form-check-input" name="answer_counter" value="<?php echo $cnt; ?>">
                            <input type="hidden" class="form-check-input" name="score_counter" value="<?php echo $score_counter; ?>">

                        </div>
                        <?php
                        if ($count > 0) {
                            while ($row = mysqli_fetch_array($query_choices)) {
                        ?>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="selected_answer" value="<?php echo $row['id']; ?>" onchange="document.getElementById('question_form').submit()">
                                        <?php echo $row['value'] ?>

                                    </label>
                                </div>

                        <?php
                            }
                        }
                        ?>
                    </form>


                </div>
            </div>
        </div>

    <?php } else { ?>

        <div class="container d-flex justify-content-center mt-4">
            <div class="card p-3 mt-3">
                <h5 class="mt-3 mb-3">Correct score <?php echo "<h2>" . $score_counter . "</h2>"; ?></h5>

                <?php if ($score_counter >= 6 && $score_counter <= 10) { ?>

                    <div class="border p-2 rounded d-flex flex-row align-items-center">
                        <div class="p-1 px-4 d-flex flex-column align-items-center score rounded"> <span class="d-block char text-success">A</span> <span class="text-success">98%</span> </div>
                        <div class="ml-2 p-3">
                            <h6 class="heading1">Congratulations!</h6> <span>You passed the quiz.</span>
                        </div>
                    </div>
                <?php } elseif ($score_counter >= 1 && $score_counter <= 5) { ?>
                    <div class="border p-2 rounded d-flex flex-row align-items-center mt-2">
                        <div class="p-1 px-4 d-flex flex-column align-items-center speed rounded"> <span class="d-block char text-warning">C</span> <span class="text-warning">72%</span> </div>
                        <div class="ml-2 p-4">
                            <h6 class="text">Low Score</h6> <span>Read more :)</span>
                        </div>
                    </div>
                <?php } ?>
                <a href="index.php" class="btn btn-primary">Restart</a>
            </div>
        </div>

    <?php } ?>



</body>

</html>