<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Quiz</title>
</head>

<body>
    <br>
    <center>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Quiz | 10 questions about video games and movies
                </div>
                <div class="card-body">
                    <form method="POST" action="quiz.php">
                        <div class="form-group">
                            <label for="username">Enter username to start the quiz</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" required="true">
                        </div>

                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </center>
</body>

</html>