<html>

<head>
    <title>Change Image on Mouseover</title>
</head>

<body>
    <img src="../uploads/userpic.png" id="img1">
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#img1').addEventListener("mouseover", function() {
            document.getElementById('img1').src = "../uploads/femalepic.jpg";
        })
        document.querySelector('#img1').addEventListener("mouseout", function() {
            document.getElementById('img1').src = "../uploads/userpic.png";
        })
    });

</script>