<html>

<head>
    <title>Alert fine name</title>
</head>

<body>
    <img id="img1" src="../uploads/userpic.png">
    <br><br>
    <div id="mylabels">
    </div>
</body>

</html>
<script>
document.addEventListener("DOMContentLoaded", function() {
    document.querySelector('#img1').addEventListener("click", function() {
        var fullPath = document.getElementById("img1").src;
        var filename = fullPath.replace(/^.*[\\\/]/, '');
        alert(filename);
    });
});

</Script>