<html>

<head>
    <title>Show current date and time in real time</title>
</head>

<body>
    <center>
        <h3>
            Real Time: 
        </h3>
        <label id="datetime_now"></label>
        <br>
        <h3>
            Refresh Every Minute: 
        </h3>
        <label id="datetime_every_minute"></label>
    </center>
</body>

</html>
<script>
    function getDateTime() {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        var hour = now.getHours();
        var minute = now.getMinutes();
        var second = now.getSeconds();
        if (month.toString().length == 1) {
            month = '0' + month;
        }
        if (day.toString().length == 1) {
            day = '0' + day;
        }
        if (hour.toString().length == 1) {
            hour = '0' + hour;
        }
        if (minute.toString().length == 1) {
            minute = '0' + minute;
        }
        if (second.toString().length == 1) {
            second = '0' + second;
        }
        var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
        return dateTime;
    }

    setInterval(function() {
        currentTime = getDateTime();
        document.getElementById("datetime_now").innerHTML = currentTime;
    }, 1000);
    
    document.getElementById("datetime_every_minute").innerHTML = getDateTime();
    setInterval(function() {
        currentTime = getDateTime();
        document.getElementById("datetime_every_minute").innerHTML = currentTime;
    }, 60000);
</script>