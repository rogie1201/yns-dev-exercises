<html>

<head>
</head>

<body>
    <div style="text-align:center;padding:10px;">
        <h1>Exercise 2-2 Page</h1>
        <input type="button" value="Go to other page" id="goto_page">
    </div>
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
    let button = document.querySelector('#goto_page');
    button.addEventListener("click", function() {
        var ok = confirm("Go to YNS page?");
        if (ok == true) {
            window.location.href = "https://yns.ph/";
        }
    });
});
</script>