<html>

<head>
    <title>Show Character</title>
</head>

<body>
    Input Character: <input type="text" id="char1" />
    <br><br>
    <label id="result_label"></label>
    <br>
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#char1').addEventListener("keyup", function() {
            var char1 = document.getElementById("char1").value;
            document.getElementById("result_label").innerHTML = char1;
        });
    });
</Script>