<html>

<head>
    <title>The four basic operations of arithmetic</title>
</head>

<body>
    First Number: <input type="number" id="num1" />
    Second Number: <input type="number" id="num2" />
    Operation Type:
    <select id="opt_type">
        <option value="1">Addtion</option>
        <option value="2">Substraction</option>
        <option value="3">Multiplication</option>
        <option value="4">Division</option>
    </select>

    <input type="submit" name="submit_btn" value="Compute" id="compute_number" />


    Result: <span id="result"></span>


    <br>
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#compute_number').addEventListener("click", function() {
            var num1 = document.getElementById("num1").value;
            var num2 = document.getElementById("num2").value;

            var type = document.getElementById("opt_type").value;
            var result = 0;

            switch (type) {
                case "1":
                    result = parseFloat(num1) + parseFloat(num2);
                    break;
                case "2":
                    result = parseFloat(num1) - parseFloat(num2);
                    break;
                case "3":
                    result = parseFloat(num1) * parseFloat(num2);
                    break;
                case "4":
                    result = parseFloat(num1) / parseFloat(num2);
                    break;
            }

            document.getElementById("result").innerHTML = result;
        });
    });
</script>