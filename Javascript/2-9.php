<html>

<head>
    <title>Change background Color</title>
</head>

<body>
    <button id="red">RED</button>
    <button id="orange">ORANGE</button>
    <button id="yellow">YELLOW</button>
    <button id="green">GREEN</button>
    <button id="blue">BLUE</button>
    <button id="indigo">INDIGO</button>
    <button id="violet">VIOLET</button>
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#red').addEventListener("click", function() {
            document.body.style.backgroundColor = 'red';
        });
        document.querySelector('#orange').addEventListener("click", function() {
            document.body.style.backgroundColor = 'orange';
        });
        document.querySelector('#yellow').addEventListener("click", function() {
            document.body.style.backgroundColor = 'yellow';
        });
        document.querySelector('#green').addEventListener("click", function() {
            document.body.style.backgroundColor = 'green';
        });
        document.querySelector('#blue').addEventListener("click", function() {
            document.body.style.backgroundColor = 'blue';
        });
        document.querySelector('#indigo').addEventListener("click", function() {
            document.body.style.backgroundColor = 'indigo';
        });
        document.querySelector('#violet').addEventListener("click", function() {
            document.body.style.backgroundColor = 'violet';
        });
    });
</Script>