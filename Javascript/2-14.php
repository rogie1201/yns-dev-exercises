<html>

<head>
    <title>Change Image on Mouseover</title>
</head>

<body>
    <center>
        <select id="img_list">
            <option value="1">Image 1</option>
            <option value="2">Image 2</option>
            <option value="3">Image 3</option>
            <option value="4">Image 4</option>
            <option value="5">Image 5</option>
        </select>
        <img src="../uploads/img1.png" id="img1" width="300px" height="200px;">
    </center>
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#img_list').addEventListener("change", function() {
            var type = document.getElementById("img_list").value;
            switch (type) {
                case "1":
                    document.getElementById("img1").src = "../uploads/img1.png";
                    break;
                case "2":
                    document.getElementById("img1").src = "../uploads/img2.jpg";
                    break;
                case "3":
                    document.getElementById("img1").src = "../uploads/img3.png";
                    break;
                case "4":
                    document.getElementById("img1").src = "../uploads/img4.jpg";
                    break;
                case "5":
                    document.getElementById("img1").src = "../uploads/img5.png";
                    break;
            }
        });
    });
</script>