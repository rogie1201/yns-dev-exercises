<html>

<head>
    <title>Change background Color</title>
    <style>
        #mydiv {
            width: 500px;
            height: 500px;
            background-color: rgb(0, 0, 139);
        }
    </style>
</head>

<body>
    <center>
        <div id="mydiv"></div>
</body>
</center>

</html>
<script>
   
    lerp = function(a, b, u) {
        return (1 - u) * a + u * b;
    };

    fade = function(element, property, start, end, duration) {
        var interval = 10;
        var steps = duration / interval;
        var step_u = 1.0 / steps;
        var u = 0.0;
        var theInterval = setInterval(function() {
            if (u >= 1.0) {
                clearInterval(theInterval)
            }
            var r = parseInt(lerp(start.r, end.r, u));
            var g = parseInt(lerp(start.g, end.g, u));
            var b = parseInt(lerp(start.b, end.b, u));
            var colorname = 'rgb(' + r + ',' + g + ',' + b + ')';
            el.style.setProperty(property, colorname);
            u += step_u;
        }, interval);
    };

    el = document.getElementById('mydiv'); 
    property = 'background-color'; 
    startColor = {
        r: 0,
        g: 0,
        b: 139
    }; // dark blue
    endColor = {
        r: 139,
        g: 206,
        b: 235
    }; // light blue
    fade(el, 'background-color', startColor, endColor, 1000);

   
    setTimeout(function() {
        fade(el, 'background-color', endColor, startColor, 1000);
    }, 2000);
</Script>