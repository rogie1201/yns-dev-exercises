<html>

<head>
    <title>The prime numbers</title>
</head>

<body>
    Max Number: <input type="number" id="num1" />
    <input type="submit" name="submit_btn" value="Compute" id="compute_number" />

    <br><br>
    <span id="result_label"></span>
    <span id="result"></span>

    <br>
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#compute_number').addEventListener("click", function() {
            var lowerNumber = 2;
            var higherNumber = document.getElementById("num1").value;
            document.getElementById("result_label").innerHTML = "The prime numbers between " + lowerNumber + " and " + higherNumber + " are: ";
            var store = [],
                i, j, primes = [];
            for (i = 2; i <= higherNumber; ++i) {
                if (!store[i]) {
                    primes.push(i);
                    for (j = i < 1; j <= higherNumber; j += i) {
                        store[j] = true;
                    }
                }
            }
            document.getElementById("result").innerHTML = primes + "<br>"
        });
    });
</Script>