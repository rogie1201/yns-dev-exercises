<html>

<head>
    <title>Change Size</title>
</head>

<body>
    <img src="../uploads/userpic.png" id="img1">
    <br>
    <br>
    <br>
    <input type="button" value="Small" id="small">
    <input type="button" value="Medium" id="medium">
    <input type="button" value="Large" id="large">
</body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector('#small').addEventListener("click", function() {
            document.getElementById("img1").height = "100";
            document.getElementById("img1").width = "100";
        });
        document.querySelector('#medium').addEventListener("click", function() {
            document.getElementById("img1").height = "200";
            document.getElementById("img1").width = "200";
        });
        document.querySelector('#large').addEventListener("click", function() {
            document.getElementById("img1").height = "300";
            document.getElementById("img1").width = "300";
        });
    });
</script>