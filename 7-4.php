<!doctype html>
<html lang="en">

<head>
    <title>Exer 7-4</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 15px;
    }

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {
        display: inline;
    }

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }
</style>

<body>
    <?php
    $connect = mysqli_connect("localhost", "root", "", "employee_db") or die("Cannot connect to Database");

    $sql = "SELECT id,
    CASE
        WHEN parent_id IS NULL THEN 'NULL'
        ELSE parent_id
    END AS parent_id
    FROM master_data
    ORDER BY
        CASE 
            WHEN `parent_id` IS NULL THEN id
            ELSE `parent_id`
        END
    ASC";
    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);

    ?>

    <table>
        <thead>
            <tr>
                <th colspan="100%">
                    <h4>Exercise 7-4</h6>
                </th>
            <tr>
            <tr>
                <th scope="col">id</th>
                <th scope="col">parent_id</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $cntr = 1;
            if ($count > 0) {
                while ($row = mysqli_fetch_array($query)) { ?>
                    <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $row['parent_id'] ?></td>
                    </tr>
            <?php
                    $cntr++;
                }
            }
            ?>
        </tbody>
    </table>
</body>

</html>