<html>

<head>
    <title>Show the greatest common divisor.</title>
</head>

<body>

    <form method="POST" action="">
        First Number: <input type="text" name="num1" />
        Second Number: <input type="text" name="num2" />

        <input type="submit" name="submit_btn" value="Submit" />
    </form>

    <br>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $n1 = $num1 = $_POST['num1'];
        $n2 = $num2 = $_POST['num2'];

        while ($num1 != $num2) {
            if ($num1 > $num2)
                $num1 = $num1 - $num2;
            else
                $num2 = $num2 - $num1;
        }

        echo "The greatest common divisor of $n1 and $n2 is: $num1";
    }
    ?>

</body>

</html>